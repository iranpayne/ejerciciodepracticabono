package com.iranhn2020.ejerciciodepracticabono

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.iranhn2020.ejerciciodepracticabono.databinding.ActivityMainBinding
import com.squareup.picasso.Picasso
//by Iran Payne

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //setContentView(R.layout.activity_main)

        binding.btnVerificar.setOnClickListener {

            binding.tvResultado.text = when (binding.edtAnio.text.toString().toInt()) {
                in 1930..1948 -> "Silent Generacion"
                in 1949..1968 -> "Generacion Baby Boom"
                in 1969..1980 -> "Generacion X"
                in 1981..1993 -> "Generacion Y"
                in 1994..2010 -> "Generacion Z"
                else -> "No perteneces a ninguna Generacion"
            }

            val imagen = binding.imgGeneeracion

                var drawableResource  = when (binding.edtAnio.text.toString().toInt()) {
                in 1930..1948 -> R.drawable.silent
                in 1949..1968 -> R.drawable.baby
                in 1969..1980 -> R.drawable.x
                in 1981..1993 -> R.drawable.y
                in 1994..2010 -> R.drawable.z
                else -> "No perteneces a ninguna Generacion"
            }

            imagen.setImageResource(drawableResource as Int)



        }
    }
}